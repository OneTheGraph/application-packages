f = figure();
x=[0.15:0.01:0.9]';
f1=(%pi^2*cos(%pi*x/2))/2-(4*x^(3/2))^(-1);
plot2d(x, f1,[1],leg="$f(x)/dx^2$",rect=[0.15,-10,0.9,10],axesflag=5);
xs2eps(gcf(),'lab2_graph2.eps')
